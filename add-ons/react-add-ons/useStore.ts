import { useEffect, useState } from 'react';
import { Store } from 'crankin';

export const useStore = <StoreType>(store: Store<StoreType>) => {

    const [state, setState] = useState<StoreType>(store.getState());

    useEffect(() => {
        const callbackId = store.listen((nextState) => setState(nextState as any));
        return () => store.mute(callbackId);
    }, []);

    return state;
}