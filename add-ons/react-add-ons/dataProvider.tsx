import { Store } from 'crankin';
import React, { useState, useEffect } from 'react';

export interface DataProviderProps<StoreType> {
    store: Store<StoreType>;    
}

export function DataProvider<StoreType extends unknown>(props: React.PropsWithChildren<DataProviderProps<StoreType>>) {
    const { store, children } = props;    

    const [state, setState] = useState(store.getState());

    useEffect(() => {
        const callbackId = store.listen(nextState => setState(nextState as any));
        return () => store.mute(callbackId);
    }, []);

    if (React.Children.count(children) > 1)
        return <>{React.Children.map(children, (child, i) =>
            <React.Fragment key={i}>
                {React.cloneElement(child as any, state as any)}
            </React.Fragment>
        )}</>
    else
        return <>{React.cloneElement(children as any, state as any)}</>
}