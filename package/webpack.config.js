const path = require('path')
const TerserPlugin = require('terser-webpack-plugin')

module.exports = {
  entry: path.resolve(__dirname, 'src/index.ts'),
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader', 'ts-loader']
      }
    ]
  },
  resolve: {
    extensions: ['.ts']
  },
  output: {
    library: 'crankin',
    libraryTarget: 'umd',
    filename: 'crankin.js'
  },
  devtool: "source-map",
  optimization: {
    minimizer: [new TerserPlugin({
      extractComments: false
    })]
  }
};