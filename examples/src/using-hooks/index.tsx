import React from 'react';
import ReactDOM from 'react-dom';
import TodoStore from './TodoStore';
import { useStore } from 'crankin-add-ons';

const TodoContainer = () => {

    const todoState = useStore(TodoStore);
    const remove = (todoId: number): void => TodoStore.remove(todoId);
    const toggleComplete = (todoId: number): void => TodoStore.toggleComplete(todoId);

    return (
        <>
            <TodoCreator />
            <div>
                <h6>
                    Todos
                </h6>
                {
                    todoState.todos.map((todo) => {
                        return (
                            <p key={todo.id}>
                                <h6>{todo.text}</h6>
                                <input type="checkbox" checked={todo.isComplete} onChange={() => toggleComplete(todo.id)} />
                                <button onClick={() => remove(todo.id)}>Remove</button>
                            </p>
                        )
                    })
                }
            </div>
        </>
    )
}

const TodoCreator = () => {

    let newTodoRef = React.createRef<HTMLInputElement>();

    const addTodo = () => {
        const ref = newTodoRef.current;

        if (ref && ref.value && ref.value.length > 0)
            TodoStore.add(ref.value);
    }

    const clearAll = () => TodoStore.clearAll();

    return (
        <>
            <input type="text" placeholder="New Todo" ref={newTodoRef} autoFocus={true} />
            <button onClick={() => addTodo()}>Add</button>
            <button onClick={() => clearAll()}>Clear All</button>
        </>
    )
}

ReactDOM.render(<TodoContainer />, document.getElementById("mount"));