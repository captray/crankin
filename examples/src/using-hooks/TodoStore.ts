import { Store } from 'crankin';
import { Todo } from './Todo';

export class TodoStoreState {
    todos: Todo[] = [];
}

class TodoStore extends Store<TodoStoreState> {

    constructor() {
        super("Todos");

        this.state = new TodoStoreState();        
    }

    private todoCount: number = 0;

    add(text: string): void {
        if(text && text.length > 0) {
            const nextTodoId = this.todoCount++;

            const todo = {
                id: nextTodoId,
                text: text,
                isComplete: false
            }

            this.state.todos.push(todo);
            this.emitChange();
        }
    }

    clearAll(): void {
        this.state.todos = [];
        this.emitChange();
    }

    remove(todoId: number): void {
        const todoIndex = this.state.todos.findIndex(t => t.id === todoId);
        this.state.todos.splice(todoIndex, 1);
        this.emitChange();       
    }

    toggleComplete(todoId: number): void {
        const todo = this.state.todos.find(t => t.id === todoId);
        if (todo) { 
            todo.isComplete = !todo.isComplete;
            this.emitChange();
        }
    }
}

export default new TodoStore();