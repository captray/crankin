import { Store } from 'crankin'
import { User } from './User';

class UserStoreState {
    users: User[] = [];
    selectedUser: User | null = null;
}

class UsersStore extends Store<UserStoreState> {

    constructor() {
        super('Users')

        this.state = {
            users: [],
            selectedUser: null
        }
    }

    async getAllUsers(): Promise<void> {
        const apiResponse = await fetch('https://jsonplaceholder.typicode.com/users')
        const users = await apiResponse.json();

        this.state.users = users;
        this.emitChange();
    }

    showUserDetail(user: User): void {
        this.state.selectedUser = user;
        this.emitChange();
    }
}

export default new UsersStore();